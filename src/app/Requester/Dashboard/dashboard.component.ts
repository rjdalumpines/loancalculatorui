import { Component, OnInit, OnChanges } from '@angular/core';
import { RequesterService } from "../../services/requesterService"
import { first } from 'rxjs/operators';
import { LoanService } from 'src/app/services/loanService';
import { Loan } from 'src/app/model/Domain/Loan';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit, OnChanges {

  requester: string = '';
  loading: boolean = false;


  constructor(private requesterService: RequesterService) {


  }

  ngOnChanges() {


  }

  
  onUpdate(loans: Loan[]) {

  }

 

  ngOnInit(): void {

    let requesterUniqueId = JSON.parse(localStorage.getItem('requester') ?? "");

    this.requesterService.getRequesterByUniqueId(requesterUniqueId ?? "")
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
          this.requester = result.FirstName;
        },
        error => {
          this.loading = false;
        });
  }

}
