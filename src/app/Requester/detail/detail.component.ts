import { Component, OnInit } from '@angular/core';
import { RequesterService } from "../../services/requesterService"
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})

export class DetailComponent implements OnInit {

  forms!: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  firstName: string = "";
  lastName: string = "";
  middleName: string = "";
  mobileNumber: string = "";
  emailAddress: string = "";
  titleUniqueId: string = "";
  extensionName: string = "";
  requesterUniqueId: string = "";

  constructor(private requesterService: RequesterService,
    private formBuilder: FormBuilder) {

    this.forms = this.formBuilder.group({     
      firstName: [null, [Validators.required, Validators.minLength(1)]],
      lastName: [null, [Validators.required, Validators.minLength(1)]],
      middleName: [null, [Validators.required, Validators.minLength(1)]],
      mobileNumber: [null, [Validators.required, Validators.minLength(8)]],
      extensionName: [null, [Validators.required, Validators.minLength(1)]],
      emailAddress:[],
      requesterUniqueId:[]
    });
  }

  get f() { return this.forms.controls; }

  ngOnInit(): void {

    let requesterId = JSON.parse(localStorage.getItem('requester') ?? "");
    
    this.requesterService.getRequesterByUniqueId(requesterId ?? "")
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
          this.firstName = result?.FirstName;
          this.middleName = result.MiddleName;
          this.lastName = result.LastName;
          this.mobileNumber = result.MobileNumber;
          this.emailAddress = result.EmailAddress;
          this.extensionName = result.ExtensionName;
          this.titleUniqueId = result.TitleUniqueId;
          this.requesterUniqueId = result.UniqueId;     
          console.log(result)    
        },
        error => {
          this.loading = false;
        });
  }

  onSubmit() {

    this.submitted = true;

    if (this.forms.invalid) {
      console.log(this.forms)
      return;
    }

    this.loading = true;
    this.requesterService.updateRequester(this.forms.value)
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
        },
        error => {
          this.loading = false;
        });
  }

}



