import { Component, Input, OnInit } from '@angular/core';
import { ScheduleRate } from 'src/app/model/Domain/ScheduleRate';

@Component({
  selector: 'app-amortizations',
  templateUrl: './amortizations.component.html',
  styleUrls: ['./amortizations.component.css']
})

export class AmortizationsComponent implements OnInit {
  
  @Input() schedules!: Array<ScheduleRate> 

  constructor() {
    
  }

  ngOnInit(): void {

  } 

}
