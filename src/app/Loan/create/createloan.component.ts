import { Component, OnInit, SimpleChanges, Output } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoanService } from 'src/app/services/loanService';
import { Loan } from 'src/app/model/Domain/Loan';

@Component({
  selector: 'app-createloan',
  templateUrl: './createloan.component.html',
  styleUrls: ['./createloan.component.css']
})

export class CreateLoanComponent implements OnInit {

  requsterloans!: Loan[];

  forms!: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  calculated: boolean = false;
  terms !: number;
  principal !: number;
  interestRate !: number;
  totalInterest: number = 0.00;
  monthlyAmortization: number = 0.00;
  requesterUniqueId!: string;

  constructor(private loanService: LoanService,
    private formBuilder: FormBuilder) {

    this.forms = this.formBuilder.group({
      terms: [null, [Validators.required, Validators.min(1),Validators.max(40) ]],
      principal: [null, [Validators.required, Validators.min(1)]],
      interestRate: [null, [Validators.required, Validators.min(0)]],
      totalInterest: [],
      monthlyAmortization: [],
      requesterUniqueId: [],
    });
  }


  get f() { return this.forms.controls; }

  ngOnInit(): void {

    this.requesterUniqueId = JSON.parse(localStorage.getItem('requester') ?? "");
    this.initializeList();
  }

  onSubmit() {

    this.submitted = true;
    this.calculated = false;

    if (this.forms.invalid) {
      console.log(this.forms)
      return;
    }

    this.loading = true;
    this.loanService.createLoan(this.forms.value)
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
          console.log(result);
          this.initializeList();
        },
        error => {
          this.loading = false;
        });

   
  }

  calculate(event: any) {

    console.log(event);
    this.submitted = true;

    if (this.forms.invalid) {
      console.log(this.forms);
      return;
    }

    this.calculated = true;
    this.loading = true;
    this.loanService.computeLoan(this.forms.value)
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
          console.log(result)
          this.totalInterest = result.TotalInterest;
          this.monthlyAmortization = result.MonthlyMortgage;
        },
        error => {
          this.loading = false;
        });
  }

  initializeList(): void {

    this.loanService.getLoanByRequester(this.requesterUniqueId).pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
          console.log(result)
          this.requsterloans = result.map((x: Loan) => new Loan(x.Terms,
            x.InterestRate,
            x.Principal,
            x.MonthlyAmortization,
            x.DateCreated,
            x.UniqueId));
        },
        error => {
          this.loading = false;
        });;
  }
}
