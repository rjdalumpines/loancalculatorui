import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { first } from 'rxjs/operators';
import { Loan } from 'src/app/model/Domain/Loan';
import { ScheduleRate } from 'src/app/model/Domain/ScheduleRate';
import { AmortizationDTO } from 'src/app/model/DTO/AmortizationDTO';
import { ScheduleDTO } from 'src/app/model/DTO/ScheduleDTO';
import { LoanService } from 'src/app/services/loanService';

@Component({
  selector: 'app-loanlist',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {

  @Input() loans!: Loan[];
  scheduleList!: ScheduleRate[];
  loading: boolean = false;
  update: boolean = false;
  requesterUniqueId!: string;

  constructor(private loanService: LoanService) {

  }

  ngOnInit(): void {

  }


  GetSchedule(principal: number,
    terms: number,
    interestRate: number,
    monthlyAmortization: number) {

    this.loanService.composeSchedule(new ScheduleDTO(principal, terms, monthlyAmortization, interestRate))
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);

          this.scheduleList = result.map((x: ScheduleRate) => x);
          console.log(this.scheduleList);
        },
        error => {
          this.loading = false;
        });


    console.log(`${principal} ${terms} ${interestRate} ${monthlyAmortization}`);
  }
}
