import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateLoanComponent } from './loan/create/createloan.component';
import { DashboardComponent } from './requester/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ListComponent } from './loan/list/list.component';
import { AmortizationsComponent } from './loan/amortizations/amortizations.component';
import { DetailComponent } from './requester/detail/detail.component';
import { NotfoundComponent } from './notfound/notfound.component';

@NgModule({
  declarations: [AppComponent,
    CreateLoanComponent,
    DashboardComponent,
    LoginComponent,
    ListComponent,
    AmortizationsComponent,
    DetailComponent,
    NotfoundComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
