
export class User {
    Id: string;
    Token: string;

    constructor(id: string,
        token: string) {
        this.Id = id;
        this.Token = token;
    }
}