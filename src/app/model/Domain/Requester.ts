export class Requester {

    public FirstName: string;
    public LastName: string;
    public MiddleName: string;
    public ExtensionName: string;
    public MobileNumber: string;
    public EmailAddress: string;
    public TitleUniqueId: string;
    public RequesterUniqueId: string;

    constructor(firstName: string,
        lastName: string,
        middleName: string,
        extensionName: string,
        mobileNumber: string,
        emailAddress: string,
        titleUniqueId: string,
        requesterUniqueId: string) {
            
        this.FirstName = firstName;
        this.LastName = lastName;
        this.MiddleName = middleName;
        this.ExtensionName = extensionName;
        this.MobileNumber = mobileNumber;
        this.EmailAddress = emailAddress;
        this.TitleUniqueId = titleUniqueId;
        this.RequesterUniqueId = requesterUniqueId;
    }

}