export class LoanRequester {

    EmailAddress !: string;
    FirstName!: string;
    ExtensionName!: string;
    LastName!: string;
    MiddleName!: string;
    MobileNumber !: string;
    MonthlyAmortization !: number;
    Principal  !: number;
    Terms !: number;
    TitleUniqueId!: string;
    RequesterUniqueId !: string;
    InterestRate !: number;
    LoanUniqueid  !: number;


    constructor(emailAddress: string,
        firstName: string,
        extensionName: string,
        lastName: string,
        middleName: string,
        mobileNumber: string,
        monthlyAmortization: number,
        principal: number,
        terms: number,
        titleUniqueId: string,
        requesterUniqueId: string,
        interestRate: number,
        loanUniqueid: number
    ) {

        this.EmailAddress = emailAddress;
        this.FirstName = firstName;
        this.ExtensionName = extensionName;
        this.LastName = lastName;
        this.MiddleName = middleName;
        this.MobileNumber = mobileNumber;
        this.MonthlyAmortization = monthlyAmortization;
        this.Principal = principal;
        this.Terms = terms;
        this.TitleUniqueId = titleUniqueId;
        this.RequesterUniqueId = requesterUniqueId;
        this.InterestRate = interestRate;
        this.LoanUniqueid = loanUniqueid;

    }
}