
export class ScheduleRate {

    public InterestPaid: number;
    public MonthlyAmortization: number;
    public RunningBalance: number;
    public Principal: number;
    public RunningInterest: number;

    constructor(interestPaid: number,
        monthlyAmortization: number,
        runningBalance: number,
        principal: number,
        runningInterest: number) {

        this.InterestPaid = interestPaid;
        this.MonthlyAmortization = monthlyAmortization;
        this.RunningBalance = runningBalance;
        this.Principal = principal;
        this.RunningInterest = runningInterest;
    }
}
