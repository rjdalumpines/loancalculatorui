export class Loan {

    public Terms: number;
    public InterestRate: number;
    public Principal: number;
    public MonthlyAmortization: number;
    public DateCreated: Date;
    public UniqueId: string;

    constructor(public terms: number,
        interestRate: number,
        principal: number,
        monthlyAmortization: number,
        dateCreated: Date,
        uniqueId: string) {

        this.Terms = terms;
        this.InterestRate = interestRate;
        this.Principal = principal;
        this.MonthlyAmortization = monthlyAmortization;
        this.DateCreated = dateCreated;
        this.UniqueId = uniqueId;
    }
}