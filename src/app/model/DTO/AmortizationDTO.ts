export class AmortizationDTO {

    Principal: number;
    Terms: number;
    InterestRate: number;
    
    constructor(principal: number,
        terms: number,
        interestRate:number) {

        this.Terms = terms;
        this.Principal = principal;
        this.InterestRate = interestRate;
    }
}