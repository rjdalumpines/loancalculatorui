export class ScheduleDTO {

    Principal: number;
    Terms: number;
    MonthlyAmortization: number;
    Interest: number;
    
    constructor(principal: number,
        terms: number,
        monthlyAmortization: number,
        interestRate:number) {

        this.Terms = terms;
        this.Principal = principal;
        this.MonthlyAmortization = monthlyAmortization;
        this.Interest = interestRate;
    }
}