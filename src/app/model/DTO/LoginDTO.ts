export class LoginDTO {
    
    EmailAddress: string;
    Password: string;

    constructor(emailAddress: string,
        password: string) {

        this.Password = password;
        this.EmailAddress = emailAddress;

    }
}