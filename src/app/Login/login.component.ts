import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/userService';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  forms!: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  returnUrl!: string;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router) {

  }

  ngOnInit(): void {

    this.forms = this.formBuilder.group({
      emailAddress: [null, [Validators.required, Validators.minLength(11)]],
      password: [null, [Validators.required, Validators.minLength(6)]]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
  }

  get f() { return this.forms.controls; }

  onSubmit() {

    this.submitted = true;

    if (this.forms.invalid) {
      console.log(this.forms)
      return;
    }

    this.loading = true;
    this.userService.login(this.forms.value)
      .pipe(first())
      .subscribe(
        data => {
          let resultJSON = JSON.stringify(data);
          let result = JSON.parse(resultJSON);
          console.log(result);
          localStorage.setItem('requester', JSON.stringify(result.ResultData.RequesterUniqueId))
          this.router.navigate(['/dashboard']);
        },
        error => {
          this.loading = false;
        });
  }
}


