import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Requester } from "../model/Domain/Requester";
import { HttpClient } from "@angular/common/http";
import Settings from "../../assets/config.json"

@Injectable({
    providedIn: 'root'
})

export class RequesterService {

    private baseURL!: string;
    private getByidURL!: string;
    private update!: string;

    constructor(private http: HttpClient) {

        this.baseURL = Settings.Config.loancalculatorAPI.URL;
        this.getByidURL = Settings.Config.requester.byId;
        this.update = Settings.Config.requester.update;
    }

    getRequesterByUniqueId(requesterId: string): Observable<Requester> {

        return this.http.get<Requester>(`${this.baseURL}/${this.getByidURL}/${requesterId}`);
    }

    updateRequester(requester: Requester): Observable<Requester> {
        console.log(requester);

        return this.http.put<Requester>(`${this.baseURL}/${this.update}`, requester);
    }
}