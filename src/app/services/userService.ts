import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginDTO } from '../model/DTO/LoginDTO';
import Settings from "../../assets/config.json"

@Injectable({ providedIn: 'root' })

export class UserService {

    private baseURL!: string;
    private loginURL!: string;
    private registerURL!: string;

    constructor(private http: HttpClient) {
        this.baseURL = Settings.Config.loancalculatorAPI.URL;
        this.loginURL = Settings.Config.user.logIn;
        this.registerURL = Settings.Config.user.register;
    }

    register(user: LoginDTO) {
        return this.http.post(`${this.baseURL}/${this.registerURL}`, user);
    }

    login(user: LoginDTO) {
        return this.http.post(`${this.baseURL}/${this.loginURL}`, user);
    }
}
