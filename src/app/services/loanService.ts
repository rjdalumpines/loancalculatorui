import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Loan } from "../model/Domain/Loan";
import { HttpClient } from "@angular/common/http";
import { AmortizationDTO } from "../model/DTO/AmortizationDTO";
import Settings from "../../assets/config.json"
import { LoanRequester } from "../model/Domain/LoanRequester";
import { ScheduleDTO } from "../model/DTO/ScheduleDTO";
import { ScheduleRate } from "../model/Domain/ScheduleRate";

@Injectable({
    providedIn: 'root'
})

export class LoanService {

    private baseURL!: string;
    private getByidURL!: string;
    private computeRatesURL!: string;
    private createURL!: string;
    private scheduleURL!: string;

    constructor(private http: HttpClient) {
        this.baseURL = Settings.Config.loancalculatorAPI.URL;
        this.getByidURL = Settings.Config.loan.byId;
        this.createURL = Settings.Config.loan.create;
        this.computeRatesURL = Settings.Config.loan.compute;
        this.scheduleURL = Settings.Config.loan.schedule;
    }

    getLoanByRequester(requesterId: string): Observable<Loan[]> {

        return this.http.get<Loan[]>(`${this.baseURL}/${this.getByidURL}/${requesterId}`);
    }

    createLoan(request: AmortizationDTO): Observable<LoanRequester> {

        console.log(request);
        return this.http.post<LoanRequester>(`${this.baseURL}/${this.createURL}`, request);
    }

    computeLoan(request: AmortizationDTO): Observable<Loan> {

        console.log(request);
        return this.http.post<Loan>(`${this.baseURL}/${this.computeRatesURL}`, request);
    }

    composeSchedule(request: ScheduleDTO): Observable<ScheduleRate[]> {

        console.log(request);
        return this.http.post<ScheduleRate[]>(`${this.baseURL}/${this.scheduleURL}`, request);
    }
}