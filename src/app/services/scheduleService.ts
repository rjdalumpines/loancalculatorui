import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ScheduleRate } from "../model/Domain/ScheduleRate";
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})

export class ScheduleService {

    private loancalculatorAPI_URL!: string;

    constructor(private http: HttpClient) {

    }

    getPaymentSchedule(): Observable<ScheduleRate[]> {

        return this.http.get<ScheduleRate[]>(this.loancalculatorAPI_URL,{
            params:{

            }
        });
    }
}


